<?php
class Config
{
    private static $instance = NULL;
    /* Database connection */
    private $db = '';
    private $db_username = '';
    private $db_password = '';
    private $dbname = '';
    private $db_hostname = '127.0.0.1';

    private $siteName = ""; /* Your site name */
    private $siteRoot = ""; /* Site URL, INCLUDING trailing slash ( e.g. www.mysite.com/ ) */


    function __construct()
    {
        $this->db = new mysqli($this->db_hostname, $this->db_username, $this->db_password, $this->dbname);

        if ($this->db->connect_error) {
            die('mysqli connect error (' . $this->db->connect_errno .')' . $this->db->connect_error);
        }
        $this->db->set_charset("UTF8");
    }


    function getDatabaseInfo()
    {  
        return array(
            'user' => $this->db_username,
            'host' => $this->db_hostname,
            'password' => $this->db_password,
            'database' => $this->dbname
        );
    }


    function getDatabaseConnection() { return $this->db; }
    function getSiteName() { return $this->siteName; }
    function getSiteRoot() { return $this->siteRoot; }


    static function getInstance()
    {
        if(!self::$instance)
            self::$instance = new Config(); 
        return self::$instance;
    }
}
