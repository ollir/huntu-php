<?php
include(__DIR__ . "/dirs.php");
require_once(ROOT . "core/Application.php");

session_start();

ini_set('display_errors', 'On');
error_reporting(E_ALL);

$app = Application::getInstance();
$app->handleRequest($_GET);
