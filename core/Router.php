<?php

class Router {
    private $controller;
    private $route;
    private $requestUrl;

    function __construct($requestUrl)
    {
        $this->requestUrl = $requestUrl;
        /* Default route is home if none was specified */
        if (count($requestUrl) == 0) {
            $this->requestUrl['route'] = 'home';
        }
        $this->route = $this->requestUrl['route'];
        
        /* Default action is "index" */
        if (!isset($this->requestUrl['action']))
            $this->requestUrl['action'] = 'index';

        $controllerName = ucwords($this->route);
        $controllerClass = $controllerName . "Controller";
        $this->controller = new $controllerClass();
    }


    /*
     * Calls the controller function specified in the request URL parameter "action"
     */
    function invokeController()
    {
        if (isset($this->requestUrl['action'])) {
            $view = call_user_func_array(
                array($this->controller,
                $this->requestUrl['action']),
                array_slice($this->requestUrl, 2));

            return $view;
        }
    }


    function getController() 
    {
        return $this->controller;
    }
}
