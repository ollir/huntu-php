<?php

class Session
{
    private $user;
    static private $instance;

    public function __construct()
    {
        if (isset($_SESSION['userId']))
            $this->user = User::existingWithId($_SESSION['userId']);
        else
            $this->user = null;
    }


    static function getInstance()
    {
        if(!self::$instance)
            self::$instance = new Session(); 
        
        return self::$instance;
    }
    
    
    public function getUser()
    {
        return $this->user;
    }


    /*
     * Just a shortcut to Session::getInstace()->getUser()->getId()
     */
    public function getUserId()
    {
        if (isset($_SESSION['userId']))
            return $_SESSION['userId'];
        else
            return null;
    }

}
