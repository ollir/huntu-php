<?php
class View
{
    protected $model;
    protected $pageTitle;
    protected $scripts; /* array of filenames for scripts to load in <head> */
    protected $beforeContent; /* For injecting html/php before actual page content rendering */
    protected $content = array(); /* Array of filenames of pages to render */

    function __construct($page, $model = null)
    {
        $this->model = $model;
        $this->pageTitle = NULL;
        $this->scripts = array();
        $this->onLoad = null;
        $this->beforeContent = null;
        array_push($this->content, $page);

        $this->addScripts(array("view/html/js/jquery-2.2.0.min.js"));
    }


    /*
     * Sends html <head>, then renders a common header, content, and finally a footer.
     * Any variables local to this function are visible in the html/php files.
     */    
    public function render($mainContent = null)
    {
        ini_set('display_errors', 'On');
        error_reporting(E_ALL);

        $user = Session::getInstance()->getUser();

        $this->htmlHeader();
        echo '<body>' . PHP_EOL;        
        
        $this->renderPageHeader();

        /* Prepare container div for rendering content */ 
        echo '<main>' . PHP_EOL;

        if ($this->beforeContent != null) {
            if ($this->beforeContent)
                require(ROOT . "view/html/" . $this->beforeContent);
        }

        /* Render content */
        foreach ($this->content as $page)
            require(ROOT . "view/html/" . $page);
        
        echo '</main>' . PHP_EOL;

        /* Render footer and close html document */
        $this->renderFooter();
        echo '</body>' . PHP_EOL;
        echo '</html>' . PHP_EOL;
    }


    private function htmlHeader()
    {
        require(ROOT . "view/html/htmlHeader.php");
    }    


    private function renderPageHeader()
    {
        require(ROOT . "view/html/header.php");
    }


    private function renderFooter()
    {
        require(ROOT . "view/html/footer.php");
    }


    public function setPageTitle($title) { $this->pageTitle = $title; }
    public function getPageTitle() { return $this->pageTitle; }


    public function addScripts($scripts)
    {
        foreach ($scripts as $script)
            array_push($this->scripts, $script);
    }


    public function addContent($page)
    {
        array_push($this->content, $page);
    }

    
    public function beforeContent($file)
    {
        $this->beforeContent = $file;
    }

    
    public function addViewVars($vars)
    {
        foreach ($vars as $var => $val) {
            $this->${"var"} = $val;
        }
    }
}
