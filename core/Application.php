<?php
class Application
{
    private $isInit = false;
    private static $instance;

    function __construct()
    {
        if ($this->isInit == TRUE)
            return;
        spl_autoload_register(array($this, 'autoload'));
        $this->isInit = TRUE;
    }

    
    static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new Application();

        return self::$instance;
    }


    /* This is the main function to handle the request->response cycle */
    function handleRequest($request)
    {
        $router = new Router($request);
        $view = $router->invokeController();
        $view->render();
    }


    private function autoload($className) 
    {
        $dirs = array();
        $dirs = ['core', 
                 'model',
                 'model/entity',
                 'controller',
                 'view'];
        $filename = $className . ".php";
        /* Check first in the root directory */
        if(file_exists(ROOT . $filename)) {
            require(ROOT . $filename);
            return;
        }
        foreach($dirs as $dir) {
            $fullPath = ROOT . "/" . $dir . "/" . $filename;
            if (file_exists($fullPath)) {
                require $fullPath;
                break;
            }
        }
    }
}
