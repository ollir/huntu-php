<?php

class Loader
{
    function loadModel($modelName, $args = array()) {
        $modelFile = ROOT . "model/" . ucwords($modelName) . "Model.php";
        $modelClass = ucwords($modelName) . "Model";
        require_once($modelFile);
        return new $modelClass($args);
    }
}
